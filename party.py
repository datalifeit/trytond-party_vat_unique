# This file is part party_vat_unique module for Tryton.  The COPYRIGHT file at
# the top level of this repository contains the full copyright notices and
# license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import Exclude
from sql.operators import Equal


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    @classmethod
    def copy(cls, parties, default=None):
        if default is None:
            default = {}
        default['identifiers'] = None
        return super(Party, cls).copy(parties, default=default)


class PartyIdentifier(metaclass=PoolMeta):
    __name__ = 'party.identifier'

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)
        table = cls.__table_handler__(module_name)

        # Drop number_uniq constraint
        table.drop_constraint('number_uniq')
        table.drop_constraint('number_excl')

    @classmethod
    def __setup__(cls):
        super(PartyIdentifier, cls).__setup__()
        t = cls.__table__()

        types = Pool().get('party.party').tax_identifier_types()
        cls._sql_constraints += [
            ('tax_code_excl', Exclude(t, (t.type, Equal), (t.code, Equal),
                where=(t.type.in_(types))),
                'party_vat_unique.msg_party_identifier_number_uniq'),

        ]

    @staticmethod
    def default_type():
        return 'eu_vat'


class PartyReplace(metaclass=PoolMeta):
    __name__ = 'party.replace'

    @classmethod
    def fields_to_replace(cls):
        fields_to_replace = super(PartyReplace, cls).fields_to_replace()
        pidentifier = ('party.identifier', 'party')
        if pidentifier not in fields_to_replace:
            fields_to_replace.append(pidentifier)
        return fields_to_replace
